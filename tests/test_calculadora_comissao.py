from unittest import TestCase
import sistema_vendas.calculadora_comissao as calculadora_comissao

#Nossa primeira história de usuário:

#Eu, enquanto vendedor, gostaria de calcular o
# valor da comissão de vendas para uma venda.

#valor da venda <= 10k: comissão de 5%
#valor da venda > 10k: comissão de 6%

#Pilotis e Navegadoris
#Lula
#luan
#robson lourenço
#renan
#jon
#lacerda
class TestCalculadoraComissao(TestCase):
    def test_calculando_comissao_de_venda_7500_deve_retornar_375(self):
        #arrange
        venda = 7500
        comissao = 375

        #act
        result = calculadora_comissao.calcular(venda)

        #assert
        self.assertEqual(result, comissao)

    def test_calculando_comissao_de_venda_7000_deve_retornar_350(self):
        #arrange
        venda = 7000
        comissao = 350

        #act
        result = calculadora_comissao.calcular(venda)

        #assert
        self.assertEqual(result, comissao)

    def test_calculando_comissao_de_venda_10000_deve_retornar_500(self):
        #arrange
        venda = 10000
        comissao = 500

        #act
        result = calculadora_comissao.calcular(venda)

        #assert
        self.assertEqual(result, comissao)

    def test_calculando_comissao_de_venda_11000_deve_retornar_comissao_de_660_reais(self):
        #arrange
        venda = 11000
        comissao = 660

        #act
        result = calculadora_comissao.calcular(venda)

    def test_calculando_comissao_de_venda_12000_deve_retornar_720(self):
        # arrange
        venda = 12000
        comissao = 720

        # act
        result = calculadora_comissao.calcular(venda)

        #assert
        self.assertEqual(result, comissao)

    def test_calculando_comissao_de_venda_11000_deve_retornar_660(self):
        # arrange
        venda = 11000
        comissao = 660

        # act
        result = calculadora_comissao.calcular(venda)

        #assert
        self.assertEqual(result, comissao)

    def test_calculando_comissao_de_venda_55_59_deve_retornar_2_77(self):
        # arrange
        venda = 55.59
        comissao = 2.77

        # act
        result = calculadora_comissao.calcular(venda)

        #assert
        self.assertEqual(result, comissao)



