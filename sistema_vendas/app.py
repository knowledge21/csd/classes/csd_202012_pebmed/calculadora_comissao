from flask import Flask, escape, request

import sistema_vendas.calculadora_comissao as calculadora_comissao

app = Flask(__name__)

@app.route('/comissao/<float:venda>')
def comissao(venda):
    return '{:.2f}'.format(calculadora_comissao.calcular(venda))