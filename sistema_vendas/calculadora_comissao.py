import math

LIMITE_SUPERIOR_FAIXA_1_DE_COMISSOES = 10000

def calcular(venda):

    if venda <= LIMITE_SUPERIOR_FAIXA_1_DE_COMISSOES:
        comissao = math.floor(venda * 0.05 * 100)/100
    else:
        comissao = venda * 0.06

    return comissao